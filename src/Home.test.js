import React from "react";
import Home from "Home";
import TestRenderer from "react-test-renderer";
import { Router } from "react-router-dom";
import { createBrowserHistory } from "history";

const act = TestRenderer.act;

it("render page", () => {
  var hist = createBrowserHistory();

  act(() => {
    TestRenderer.create(
      <Router history={hist}>
        <Home />
      </Router>
    );
  });
});
