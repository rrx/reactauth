import React, { useState } from "react";
import { SetS3Config } from "./services";
import Storage from "@aws-amplify/storage";
import { S3Image } from "aws-amplify-react";
import { makeStyles } from "@material-ui/core/styles";
import classnames from "classnames";

import Header from "components/Header/Header";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import CardFooter from "components/Card/CardFooter.js";
import SimpleCard from "pages/SimpleCard/SimpleCard";
import Button from "@material-ui/core/Button";
import Avatar from "@material-ui/core/Avatar";

import pageStyles from "styles";

const useStyles = makeStyles({
  zoom: {
    //transition: 'transform .2s', /* Animation */
    "&:hover": {
      transform:
        "scale(5.5)" /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
    }
  },
  avatar1: {
    margin: 10
  },
  avatar2: {
    margin: 10,
    width: 60,
    height: 60
  },
  avatar3: {
    margin: 10,
    width: 100,
    height: 100,
    objectFit: "cover"
  },
  photoImg: {
    height: "100px",
    width: "100px",
    objectFit: "cover",
    borderRadius: "50%"
  }
});

export default function Upload() {
  const [state, setState] = useState({
    imageName: "",
    imageFile: "",
    response: "",
    uploading: false,
    upload: null
  });

  const styles = useStyles();

  const uploadImage = (key, file) => {
    SetS3Config(process.env.REACT_APP_AWS_S3_BUCKET_UPLOAD, "protected");
    setState({ ...state, uploading: true });
    Storage.put(key, file, {
      contentType: file.type
    })
      .then(result => {
        setState({ ...state, upload: null });
        console.log(result);
        setState({ response: "Success uploading file!" });
      })
      .catch(err => {
        setState({ response: `Cannot uploading file: ${err}` });
      })
      .finally(() => {
        setState({ ...state, uploading: false });
      });
  };

  const classes = pageStyles();
  return (
    <div>
      <Header />
      <SimpleCard>
        <CardHeader color="primary" className={classes.cardHeader}>
          <h4>S3 Upload example</h4>
        </CardHeader>
        <CardBody>
          {!!state.response && <div>{state.response}</div>}

          <Button variant="contained" component="label">
            Upload File
            <input
              type="file"
              accept="image/png, image/jpeg"
              style={{ display: "none" }}
              onChange={e => {
                e.persist();
                const file = e.target.files[0];
                console.log("e", e, file);
                uploadImage("profile", file);
              }}
            />
          </Button>

          {!state.uploading && (
            <div>
              <Avatar
                alt="Profile Small"
                className={classnames(styles.avatar1, styles.zoom)}
              >
                <S3Image imgKey="profile" level="protected" />
              </Avatar>

              <Avatar alt="Profile Big" className={styles.avatar2}>
                <S3Image imgKey="profile" level="protected" />
              </Avatar>

              <Avatar alt="Profile Bigger" className={styles.avatar3}>
                <S3Image imgKey="profile" level="protected" />
              </Avatar>

              <S3Image
                imgKey="profile"
                level="protected"
                theme={{
                  photoImg: {
                    height: "100px",
                    width: "100px",
                    objectFit: "cover",
                    borderRadius: "50%"
                  }
                }}
              />
            </div>
          )}
        </CardBody>
        <CardFooter></CardFooter>
      </SimpleCard>
    </div>
  );
}
