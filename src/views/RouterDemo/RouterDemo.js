import React from "react";
import PropTypes from "prop-types";
import Header from "components/Header/Header";
import pageStyles from "styles";
import { Route, Link } from "react-router-dom";

import SimpleCard from "pages/SimpleCard/SimpleCard";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import CardFooter from "components/Card/CardFooter.js";

function RouterDemoItem({ match }) {
  return (
    <div>
      <h3>Requested Param: {match.params.id}</h3>
    </div>
  );
}

RouterDemoItem.propTypes = {
  match: PropTypes.object.isRequired
};

export default function RouterDemo() {
  const classes = pageStyles();
  return (
    <div>
      <Header />
      <SimpleCard>
        <CardHeader color="primary" className={classes.cardHeader}>
          <h4>Router Demo</h4>
        </CardHeader>
        <CardBody>
          <ul>
            <li>
              <Link to="/routerdemo/route1">Route 1</Link>
            </li>
            <li>
              <Link to="/routerdemo/route2">Route 2</Link>
            </li>
          </ul>
          <Route path="/routerdemo/:id" component={RouterDemoItem} />
          <Route
            exact
            path="/routerdemo"
            render={() => <h3>Please select an item.</h3>}
          />
        </CardBody>
        <CardFooter></CardFooter>
      </SimpleCard>
    </div>
  );
}
