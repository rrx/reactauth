import React from "react";
import { Link } from "react-router-dom";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import { makeStyles } from "@material-ui/core/styles";

export default function Home() {
  const classes = useStyles();

  return (
    <div className={classes.appBody}>
      <GridContainer justify="center">
        <GridItem xs={12} sm={12} md={8}>
          <p className={classes.description}>
            This is a responsive demonstration app written in{" "}
            <a href="https://reactjs.org/">React</a>
          </p>
          <p>
            Public Repo at:{" "}
            <a
              target="_blank"
              rel="noopener noreferrer"
              href="https://gitlab.com/rrx/reactauth"
            >
              https://gitlab.com/rrx/reactauth
            </a>
          </p>
          <p>Features include:</p>
          <ul>
            <li>
              CI/CD using{" "}
              <a href="https://aws-amplify.github.io/">AWS Amplify</a> and{" "}
              <a href="https://gitlab.com">Gitlab</a>
            </li>
            <li>
              Authentication using{" "}
              <a href="https://aws-amplify.github.io">AWS Amplify</a>
            </li>
            <li>
              <Link to="/totpsetup">Multi-Factor Authentication (MFA)</Link>
            </li>
            <li>Password Recovery via email</li>
            <li>
              Mobile friendly responsive design using{" "}
              <a href="https://material-ui.com">Material Design</a>
            </li>
            <li>
              Clean urls and routing using{" "}
              <a href="https://reacttraining.com/react-router/">React Router</a>
            </li>
            <li>
              Using{" "}
              <a href="https://reactjs.org/docs/hooks-intro.html">
                React Hooks
              </a>
            </li>
          </ul>
        </GridItem>
      </GridContainer>
    </div>
  );
}

const useStyles = makeStyles(theme => ({
  appBody: {
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1)
  }
}));
