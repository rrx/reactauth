import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";

import InfoOutlined from "@material-ui/icons/InfoOutlined";
import IconButton from "@material-ui/core/IconButton";
import { useUserState } from "state/userState";
import { withRouter } from "react-router-dom";
import Popover from "@material-ui/core/Popover";

const AlwaysOn = props => {
  const [userState] = useUserState();
  console.log("always", props, userState);
  const user = userState.user;
  const emailVerified = ((user || {}).attributes || {}).email_verified || false;
  const email = ((user || {}).attributes || {}).email;

  return (
    <div>
      {userState.user && (
        <div>
          User logged in. Email {emailVerified ? "verified" : "not verified"}
        </div>
      )}

      {!userState.user && <div>User logged out</div>}

      <div>Location: {props.location.pathname}</div>
      <div>Email: {email}</div>
    </div>
  );
};

AlwaysOn.propTypes = {
  location: PropTypes.object.isRequired
};

const AlwaysOnRoute = withRouter(AlwaysOn);

export default function DebugPopup() {
  const [open, setOpen] = React.useState(null);
  const classes = useStyles();

  function onClick(e) {
    console.log("e", e);
    if (open) {
      setOpen(null);
    } else {
      setOpen(e.currentTarget);
    }
  }

  return (
    <span className={classes.container}>
      <IconButton aria-label="debug" onClick={onClick}>
        <InfoOutlined color={open ? "action" : "primary"} />
      </IconButton>
      <Popover
        classes={{
          paper: classes.popover
        }}
        open={open != null}
        anchorEl={open}
        onClose={() => setOpen(null)}
        anchorOrigin={{
          vertical: "center",
          horizontal: "left"
        }}
        transformOrigin={{
          vertical: "center",
          horizontal: "right"
        }}
      >
        <AlwaysOnRoute />
      </Popover>
    </span>
  );
}

const useStyles = makeStyles({
  container: {
    position: "fixed",
    bottom: 0,
    left: 20,
    zIndex: 1000
  },
  popover: {
    padding: 10
  }
});
