import React, { useState } from "react";
import Header from "components/Header/Header";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import CardFooter from "components/Card/CardFooter.js";
import SimplePage from "pages/SimplePage/SimplePage";
import pageStyles from "styles";

import { SetS3Config } from "./services";
import Storage from "@aws-amplify/storage";
import { S3Image } from "aws-amplify-react";
import { PhotoPicker } from "aws-amplify-react";

const previewStyle = {
  width: 30,
  height: 30,
  objectFit: "cover",
  borderRadius: "50%"
};

function ProfileUploader() {
  const [state, setState] = useState({
    previewSrc: null,
    response: null,
    upload: null,
    uploading: false
  });
  const classes = pageStyles();

  console.log("state", state);

  const s3upload = (key, file) => {
    console.log("s3upload", key, file);
    SetS3Config(process.env.REACT_APP_AWS_S3_BUCKET_UPLOAD, "protected");
    //setState({ ...state, previewSrc: url }
    setState({ ...state, response: null, uploading: true });
    Storage.put(key, file, {
      contentType: file.type
    })
      .then(result => {
        console.log("result", result);
        setTimeout(() => {
          setState({
            ...state,
            upload: null,
            uploading: false,
            response: "Success uploading file!"
          });
        }, 0);
      })
      .catch(err => {
        setState({
          ...state,
          response: `Cannot uploading file: ${err}`,
          uploading: false
        });
      });
  };

  function imageUpload(data) {
    console.log("onPick", data);
    s3upload("test", data.file);
  }

  return (
    <div>
      <Header />
      <SimplePage>
        <CardHeader color="primary" className={classes.cardHeader}>
          <h4>
            S3 Profile Uploader
            <div style={{ float: "right" }}>
              {state.previewSrc && (
                <img
                  alt="profile"
                  src={state.previewSrc}
                  style={previewStyle}
                />
              )}

              {!state.previewSrc && (
                <S3Image
                  imgKey="test"
                  level="protected"
                  theme={{
                    photoImg: previewStyle
                  }}
                />
              )}
            </div>
          </h4>
        </CardHeader>
        <CardBody>
          <PhotoPicker
            title="My Photo Picker"
            preview
            onLoad={url => setState({ ...state, previewSrc: url })}
            onPick={data => imageUpload(data)}
          />

          {state.response && <p>{state.response}</p>}

          {state.previewSrc && (
            <img
              alt="profile"
              src={state.previewSrc}
              style={{
                height: "100px",
                width: "100px",
                objectFit: "cover",
                borderRadius: "50%"
              }}
            />
          )}

          {!state.previewSrc && (
            <S3Image
              imgKey="test"
              level="protected"
              theme={{
                photoImg: {
                  height: "100px",
                  width: "100px",
                  objectFit: "cover",
                  borderRadius: "50%"
                }
              }}
            />
          )}
        </CardBody>
        <CardFooter></CardFooter>
      </SimplePage>
    </div>
  );
}

export default ProfileUploader;
