import React from "react";

import clsx from "clsx";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { Auth } from "aws-amplify";
import { FaSignOutAlt, FaSignInAlt } from "react-icons/fa";
import { makeStyles } from "@material-ui/core/styles";
import { withRouter } from "react-router-dom";

// state
import { useHeaderState } from "state/headerState";
import { useUserState } from "state/userState";

import "assets/scss/material-kit-react.scss";

// icons and elements
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";

const LoginLogoutButton = withRouter(({ history }) => {
  const [userState, setUserState] = useUserState();

  function signOut() {
    Auth.signOut()
      .then(data => {
        setUserState({ type: "logout" });
        history.push("/");
        console.log("signed out: ", data);
      })
      .catch(err => console.log(err));
  }

  function signIn() {
    history.push("/auth/login");
  }

  if (userState.user) {
    if (userState.user.signInUserSession) {
      return (
        <Button color="inherit" onClick={signOut}>
          <FaSignOutAlt />
          Logout
        </Button>
      );
    }
  } else {
    return (
      <Button color="inherit" onClick={signIn}>
        <FaSignInAlt />
        Login
      </Button>
    );
  }
  return <div></div>;
});

export default function TopBar() {
  const [open, setOpen] = useHeaderState();
  const classes = useStyles();

  return (
    <AppBar
      position="absolute"
      className={clsx(classes.appBar, {
        [classes.appBarShift]: open
      })}
    >
      <Toolbar>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          onClick={() => setOpen(true)}
          edge="start"
          className={clsx(classes.menuButton, open && classes.hide)}
        >
          <MenuIcon />
        </IconButton>

        <Typography variant="h6" className={classes.title}>
          AIFuzz Demo
        </Typography>

        <LoginLogoutButton />
      </Toolbar>
    </AppBar>
  );
}

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  title: {
    flexGrow: 1
  },
  link: {
    color: "white"
  },
  appBar: {
    top: 0,
    position: "fixed",
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    })
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  hide: {
    display: "none"
  }
}));
