import React from "react";
import PropTypes from "prop-types";
import Header from "components/Header/Header";
import { withRouter } from "react-router-dom";

import SimpleCard from "pages/SimpleCard/SimpleCard";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";

function NotFound({ match, history, location }) {
  console.log(match, location, history);
  return (
    <div>
      <Header />
      <SimpleCard>
        <CardBody>
          <h4>Not Found</h4>
          <p>Location: {location.pathname} </p>
        </CardBody>
        <CardFooter></CardFooter>
      </SimpleCard>
    </div>
  );
}

NotFound.propTypes = {
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired
};

export default withRouter(NotFound);
