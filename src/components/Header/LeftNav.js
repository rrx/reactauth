/* eslint react/display-name: 0 */
import React from "react";
import PropTypes from "prop-types";

import Drawer from "@material-ui/core/Drawer";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import { routes } from "routes";

// state
import { useHeaderState } from "state/headerState";
import { useUserState } from "state/userState";

import "assets/scss/material-kit-react.scss";

// icons and elements
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";

export default function LeftNav() {
  const [open, setOpen] = useHeaderState();
  const [userState] = useUserState();

  const classes = useStyles();
  const theme = useTheme();

  function ListItemLink({ icon, primary, to }) {
    const renderLink = React.useMemo(
      () =>
        React.forwardRef((linkProps, ref) => (
          // With react-router-dom@^6.0.0 use `ref` instead of `innerRef`
          // See https://github.com/ReactTraining/react-router/issues/6056
          <Link to={to} {...linkProps} innerRef={ref} />
        )),
      [to]
    );

    return (
      <li>
        <ListItem onClick={() => setOpen(false)} button component={renderLink}>
          <ListItemIcon>{icon}</ListItemIcon>
          <ListItemText primary={primary} />
        </ListItem>
      </li>
    );
  }

  ListItemLink.propTypes = {
    icon: PropTypes.element.isRequired,
    primary: PropTypes.string.isRequired,
    to: PropTypes.string.isRequired
  };

  return (
    <Drawer
      className={classes.drawer}
      variant="persistent"
      anchor="left"
      open={open}
      classes={{
        paper: classes.drawerPaper
      }}
    >
      <div className={classes.drawerHeader}>
        <IconButton onClick={() => setOpen(false)}>
          {theme.direction === "ltr" ? (
            <ChevronLeftIcon />
          ) : (
            <ChevronRightIcon />
          )}
        </IconButton>
      </div>
      <Divider />
      <List>
        {routes(userState.user != null)
          .filter(value => value.icon != null)
          .map(({ key, name, icon }, index) => {
            return (
              <ListItemLink key={index} icon={icon} primary={name} to={key} />
            );
          })}
      </List>
    </Drawer>
  );
}

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0
  },
  drawerPaper: {
    width: drawerWidth
  },
  drawerHeader: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
    justifyContent: "flex-end"
  }
}));
