import React, { useState } from "react";
import PropTypes from "prop-types";
import { Formik } from "formik";

// Components
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import CardFooter from "components/Card/CardFooter.js";
import Button from "components/CustomButtons/Button.js";
import InputAdornment from "@material-ui/core/InputAdornment";
import SimpleCard from "pages/SimpleCard/SimpleCard";
import { Link, withRouter, Redirect } from "react-router-dom";
import AuthInput from "views/Auth/Components/AuthInput";

// @material-ui/icons
import Icon from "@material-ui/core/Icon";
import Email from "@material-ui/icons/Email";

import { makeStyles } from "@material-ui/core/styles";
import { Auth } from "aws-amplify";

import styles from "assets/jss/material-kit-react/views/loginPage.js";
import { primaryColor } from "assets/jss/material-kit-react";

const useStyles = makeStyles(styles);

function Login(props) {
  const [loginStatus, setLoginStatus] = useState({});
  const classes = useStyles();

  const { onSuccess } = props;

  if (loginStatus.state) {
    return <Redirect to={`/auth/${loginStatus.state}`} />;
  }

  return (
    <SimpleCard>
      <Formik
        initialValues={{ email: "", password: "" }}
        validate={props.validate}
        onSubmit={(values, { setSubmitting, setErrors, setStatus }) => {
          console.log("submit", values, setSubmitting);
          setSubmitting(false);
          setErrors({});
          setStatus(null);
          Auth.signIn(values.email, values.password)
            .then(user => {
              console.log("user", user);
              setStatus("Logged In");
              onSuccess(user);
            })
            .catch(e => {
              console.log("error", e);
              switch (e.code) {
                case "UserNotConfirmedException":
                  setLoginStatus({ state: "signupconfirm" });
                  break;
                case "PasswordResetRequiredException":
                  setLoginStatus({
                    username: values.email,
                    state: "forgotconfirm"
                  });
                  break;
                default:
                  setErrors({ form: e.message });
                  break;
              }
            })
            .finally(() => setSubmitting(false));
        }}
      >
        {({
          values,
          errors,
          status,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting
          /* and other goodies */
        }) => (
          <form className={classes.form} onSubmit={handleSubmit}>
            <CardHeader color="primary" className={classes.cardHeader}>
              <h4>Login</h4>
            </CardHeader>
            <CardBody>
              {status && <div>{status}</div>}
              {errors.form && errors.form}

              <AuthInput
                id="email"
                labelText="Email..."
                autoComplete="on"
                autoFocus={true}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
                errors={errors}
                touched={touched}
                endAdornment={
                  <InputAdornment position="end">
                    <Email className={classes.inputIconsColor} />
                  </InputAdornment>
                }
              />

              <AuthInput
                id="password"
                type="password"
                labelText="Password"
                autoComplete="off"
                autoFocus={false}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
                errors={errors}
                touched={touched}
                endAdornment={
                  <InputAdornment position="end">
                    <Icon className={classes.inputIconsColor}>
                      lock_outline
                    </Icon>
                  </InputAdornment>
                }
              />
            </CardBody>
            <CardFooter className={classes.cardFooter}>
              <Button
                type="submit"
                color="primary"
                size="lg"
                disabled={isSubmitting}
              >
                Log In
              </Button>
            </CardFooter>
          </form>
        )}
      </Formik>

      <div className="links" style={localStyle.links}>
        <p>
          <Link style={localStyle.links} to="/forgot">
            Forgot your password?
          </Link>
        </p>
        <p>
          <Link style={localStyle.links} to="/signup">
            Don{"'"}t yet have an account? Sign up!
          </Link>
        </p>
      </div>
    </SimpleCard>
  );
}

const localStyle = {
  links: {
    paddingLeft: "25px",
    paddingRight: "25px",
    color: primaryColor
  }
};

export default withRouter(Login);

Login.propTypes = {
  onSuccess: PropTypes.func,
  validate: PropTypes.func
};
