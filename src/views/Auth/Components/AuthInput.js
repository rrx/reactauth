import React from "react";
import PropTypes from "prop-types";
import CustomInput from "components/CustomInput/CustomInput.js";

function AuthInput(props) {
  const autoComplete = props.autoComplete || "on";
  const autoFocus = props.autoFocus || false;
  const labelText = props.labelText || "";
  const errors = props.errors || {};
  const touched = props.touched || {};
  const type = props.type || "text";
  const endAdornment = props.endAdornment || null;

  const { id, value, onBlur, onChange } = props;

  return (
    <div>
      <CustomInput
        labelText={labelText}
        id={id}
        formControlProps={{
          fullWidth: true
        }}
        inputProps={{
          type: type,
          autoFocus: autoFocus,
          onChange: onChange,
          onBlur: onBlur,
          value: value,
          autoComplete: autoComplete,
          endAdornment: endAdornment
        }}
      />
      {errors[id] && touched[id] && errors[id]}
    </div>
  );
}

export default AuthInput;

AuthInput.propTypes = {
  id: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onBlur: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  autoComplete: PropTypes.string,
  autoFocus: PropTypes.bool,
  labelText: PropTypes.string,
  errors: PropTypes.object,
  touched: PropTypes.object,
  type: PropTypes.string,
  endAdornment: PropTypes.object
};
