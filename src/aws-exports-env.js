const awsmobile = {
    "aws_project_region": process.env.REACT_APP_AWS_REGION,
    "aws_cognito_identity_pool_id": process.env.REACT_APP_COGITO_IDENTITY_POOL_ID,
    "aws_cognito_region": process.env.REACT_APP_AWS_REGION,
    "aws_user_pools_id": process.env.REACT_APP_AWS_USER_POOLS_ID,
    "aws_user_pools_web_client_id": process.env.REACT_APP_AWS_USER_POOLS_WEB_CLIENT_ID,
    "oauth": {
        "domain": process.env.REACT_APP_AMPLIFY_AUTH_DOMAIN,
        "scope": [
            "phone",
            "email",
            "openid",
            "profile",
            "aws.cognito.signin.user.admin"
        ],
        "redirectSignIn": process.env.REACT_APP_AMPLIFY_REDIRECT_SIGNIN_URL,
        "redirectSignOut": process.env.REACT_APP_AMPLIFY_REDIRECT_SIGNOUT_URL,
        "responseType": "code"
    },
    "federationTarget": "COGNITO_USER_POOLS",
    "aws_user_files_s3_bucket": process.env.REACT_APP_AWS_S3_BUCKET_UPLOAD,
    "aws_user_files_s3_bucket_region": process.env.REACT_APP_AWS_REGION,
    "aws_appsync_graphqlEndpoint": process.env.REACT_APP_AWS_APPSYNC_GRAPHQL_ENDPOINT,
    "aws_appsync_region": process.env.REACT_APP_AWS_APPSYNC_REGION,
    "aws_appsync_authenticationType": process.env.REACT_APP_AWS_APPSYNC_AUTHTYPE,
    "aws_content_delivery_bucket": process.env.REACT_APP_AWS_CONTENT_DELIVERY_BUCKET,
    "aws_content_delivery_bucket_region": process.env.REACT_APP_AWS_CONTENT_DELIVERY_BUCKET_REGION,
    "aws_content_delivery_url": process.env.REACT_APP_AWS_CONTENT_DELIVERY_URL
};


export default awsmobile;
