import React from "react";
import PropTypes from "prop-types";

// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// core components
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Parallax from "components/Parallax/Parallax.js";
import { container, title } from "assets/jss/material-kit-react.js";

export default function SimpleLandingPage(props) {
  const classes = useStyles();
  return (
    <div className={classes.bg}>
      <Parallax
        className={classes.parallax}
        image={require("assets/img/landing-bg.jpg")}
      >
        {/* <Header /> */}

        <div className={classes.container}>
          <GridContainer>
            <GridItem xs={12} sm={12} md={12}>
              <h1 className={classes.maintitle}>{props.title}</h1>
            </GridItem>
          </GridContainer>
          <Footer whiteFont />
        </div>
      </Parallax>
      <div className={classNames(classes.main, classes.mainRaised)}>
        <div className={classes.container}>{props.children}</div>
      </div>
    </div>
  );
}

SimpleLandingPage.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.object.isRequired
};

const useStyles = makeStyles({
  bg: {
    backgroundImage: require("assets/img/landing-bg.jpg")
  },
  parallax: {},
  container: {
    //zIndex: "12",
    ...container,
    paddingTop: 25,
    paddingBottom: 25,
    marginBottom: 200
  },
  maintitle: {
    ...title,
    display: "inline-block",
    position: "relative",
    marginTop: "0px",
    minHeight: "32px",
    color: "#FFFFFF",
    textDecoration: "none"
  },
  title: {
    ...title,
    display: "inline-block",
    position: "relative",
    marginTop: "30px",
    minHeight: "32px",
    textDecoration: "none"
  },
  subtitle: {
    fontSize: "1.313rem",
    maxWidth: "500px",
    margin: "10px auto 0"
  },
  main: {
    background: "#FFFFFF",
    position: "relative"
    //zIndex: "3"
  },
  mainRaised: {
    margin: "-50vh 30px 0px",
    borderRadius: "6px",
    boxShadow:
      "0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)"
  }
});
