import React from "react";
import PropTypes from "prop-types";

export const Context = React.createContext(false);

export const StateProvider = ({ children }) => {
  return (
    <Context.Provider value={React.useState(false)}>
      {children}
    </Context.Provider>
  );
};

StateProvider.propTypes = {
  children: PropTypes.object.isRequired
};

export const withHeaderState = Component => {
  return function StateProviderWrapper(props) {
    return (
      <StateProvider>
        <Component {...props} />
      </StateProvider>
    );
  };
};

export const useHeaderState = () => React.useContext(Context);
