import React from "react";
import PropTypes from "prop-types";

import Header from "components/Header/Header";
import MFASetup from "views/Auth/Components/MFASetup";
import Login from "views/Auth/Components/Login";
import MFA from "views/Auth/Components/MFA";
import SignUp from "views/Auth/Components/SignUp";
import Forgot from "views/Auth/Components/Forgot";
import ForgotConfirm from "views/Auth/Components/ForgotConfirm";
import SignupConfirm from "views/Auth/Components/SignupConfirm";
import ChangePassword from "views/Auth/Components/ChangePassword";
import { withRouter } from "react-router-dom";

import { useUserState } from "state/userState";

//import { Hub } from 'aws-amplify'

// async function handleLogIn(user) {
//     } else if (user.challengeName === 'NEW_PASSWORD_REQUIRED') {
//         const {requiredAttributes} = user.challengeParam; // the array of required attributes, e.g ['email', 'phone_number']
//         // You need to get the new password and required attributes from the UI inputs
//         // and then trigger the following function with a button click
//         // For example, the email and phone_number are required attributes
//         const {username, email, phone_number} = getInfoFromUserInput();
//         const loggedUser = await Auth.completeNewPassword(
//             user,              // the Cognito User Object
//             newPassword,       // the new password
//             // OPTIONAL, the required attributes
//             {
//                 email,
//                 phone_number,
//             }
//         );
//     } else if (user.challengeName === 'MFA_SETUP') {
//         // This happens when the MFA method is TOTP
//         // The user needs to setup the TOTP before using it
//         // More info please check the Enabling MFA part
//         Auth.setupTOTP(user);
//     } else {
//         // The user directly signs in
//         console.log(user);
//     }
// }

const validate = values => {
  let errors = {};
  if (!values.password) {
    errors.password = "Required";
  }

  if (!values.email) {
    errors.email = "Required";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
    errors.email = "Invalid email address";
  }

  return errors;
};

export function Auth({ history, state }) {
  const [userState, dispatch] = useUserState();

  const authState = userState.authState;

  function redirectState(state) {
    history.push(`/auth/${state}`);
  }

  console.log("Auth loginState", history, authState);

  const onLoginSuccess = user => {
    dispatch({ type: "setUser", user: user });
    console.log("usersuccess", user.challengeName);
    if (
      user.challengeName === "SMS_MFA" ||
      user.challengeName === "SOFTWARE_TOKEN_MFA"
    ) {
      redirectState("challenge");
    } else {
      redirectState("loggedin");
    }
  };

  const onSignupSuccess = data => {
    console.log("signup", data);
    redirectState("signupconfirm");
  };

  const onMFASuccess = loggedInUser => {
    console.log("onMFASuccess", loggedInUser);
    redirectState("loggedin");
  };

  function selectComponent(state) {
    switch (state) {
      case "loggedin":
        history.push("/");
        break;
      case "challenge":
        return <MFA userState={userState} onSuccess={onMFASuccess} />;
      case "mfasetup":
        return <MFASetup />;
      case "signup":
        return (
          <SignUp
            userState={userState}
            validate={validate}
            onSuccess={onSignupSuccess}
          />
        );
      case "forgot":
        return <Forgot userState={userState} onSuccess={onLoginSuccess} />;
      case "forgotconfirm":
        return (
          <ForgotConfirm userState={userState} onSuccess={onLoginSuccess} />
        );
      case "signupconfirm":
        return (
          <SignupConfirm userState={userState} onSuccess={onLoginSuccess} />
        );
      case "changepassword":
        return (
          <ChangePassword userState={userState} onSuccess={onLoginSuccess} />
        );
      case "start":
      default:
        return (
          <Login
            userState={userState}
            validate={validate}
            onSuccess={onLoginSuccess}
          />
        );
    }
  }

  return (
    <div>
      <Header />
      {selectComponent(state)}
    </div>
  );
}

Auth.propTypes = {
  history: PropTypes.object.isRequired,
  state: PropTypes.string
};

export default withRouter(Auth);
