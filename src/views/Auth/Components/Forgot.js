import React, { useState } from "react";
import { Formik } from "formik";

// Components
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import CardFooter from "components/Card/CardFooter.js";
import Button from "components/CustomButtons/Button.js";
import SimpleCard from "pages/SimpleCard/SimpleCard";
import AuthInput from "views/Auth/Components/AuthInput";
import ForgotConfirm from "views/Auth/Components/ForgotConfirm";

import { makeStyles } from "@material-ui/core/styles";
import { Auth } from "aws-amplify";

import styles from "assets/jss/material-kit-react/views/loginPage.js";

const useStyles = makeStyles(styles);

function Forgot() {
  const [submitted, setSubmitted] = useState(null);

  const classes = useStyles();

  const validate = values => {
    console.log("validate", values);
    let errors = {};
    if (!values.username) {
      errors.username = "Required";
    }
    return errors;
  };

  if (submitted) {
    return <ForgotConfirm {...submitted} />;
  }

  return (
    <SimpleCard>
      <Formik
        initialValues={{ username: "" }}
        validate={validate}
        onSubmit={(values, { setSubmitting, setErrors, setStatus }) => {
          console.log("submit", values);
          setSubmitting(false);
          setErrors({});
          setStatus(null);

          Auth.forgotPassword(values.username)
            .then(data => {
              console.log("forgot", data);
              // setStatus('MFA Confirmation')
              setSubmitted({
                destination: data.CodeDeliveryDetails.Destination,
                username: values.username
              });
            })
            .catch(e => {
              console.log("Error", e);
              setErrors({ username: e.message });
            })
            .finally(() => setSubmitting(false));
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting
          /* and other goodies */
        }) => (
          <form className={classes.form} onSubmit={handleSubmit}>
            {console.log("errors", errors)}
            <CardHeader color="primary" className={classes.cardHeader}>
              <h4>Forgot Your Password</h4>
            </CardHeader>
            <CardBody>
              <AuthInput
                id="username"
                labelText="Enter your email..."
                autoComplete="off"
                autoFocus={true}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.username}
                errors={errors}
                touched={touched}
              />
            </CardBody>
            <CardFooter className={classes.cardFooter}>
              <Button
                type="submit"
                color="primary"
                size="lg"
                disabled={isSubmitting}
              >
                Submit
              </Button>
            </CardFooter>
          </form>
        )}
      </Formik>
    </SimpleCard>
  );
}

export default Forgot;
