import React, { useEffect } from "react";
import PropTypes from "prop-types";

import { Auth, Hub } from "aws-amplify";
import CssBaseline from "@material-ui/core/CssBaseline";
import { Router } from "react-router-dom";
import { ThemeProvider } from "@material-ui/styles";

import theme from "./theme";
import { Routes } from "./routes";

// import { ConsoleLogger } from "@aws-amplify/core";
import { useUserState, withUserState } from "state/userState";

function App({ history }) {
  const [, dispatch] = useUserState();

  useEffect(() => {
    // set listener for auth events
    Hub.listen("auth", data => {
      console.log(data);

      const { payload } = data;
      if (payload.event === "signIn") {
        setImmediate(() => dispatch({ type: "setUser", user: payload.data }));
      }
      // this listener is needed for form sign ups since the OAuth will redirect & reload
      if (payload.event === "signOut") {
        setTimeout(() => dispatch({ type: "setUser", user: null }), 350);
      }
    });
    // we check for the current user unless there is a redirect to ?signedIn=true
    if (!window.location.search.includes("?signedin=true")) {
      checkUser(dispatch);
    }
  }, [dispatch]);

  return (
    <Router history={history}>
      <ThemeProvider theme={theme}>
        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
        <CssBaseline />
        <div style={styles.pageContainer}>
          <div style={styles.appContainer}>
            <Routes />
          </div>
        </div>
      </ThemeProvider>
    </Router>
  );
}

App.propTypes = {
  history: PropTypes.object.isRequired
};

async function checkUser(dispatch) {
  try {
    const user = await Auth.currentAuthenticatedUser();
    console.log("user: ", user);
    user.getSession(function(err, session) {
      if (err) {
        console.log("err: ", err);
      } else {
        console.log("session: ", user, session);
        dispatch({ type: "setUser", user });
      }
    });
  } catch (err) {
    console.log("err: ", err);
    dispatch({ type: "loaded" });
  }

  Auth.currentSession()
    .then(data => console.log("session", data))
    .catch(err => console.log(err));
}

const styles = {
  pageContainer: {
    paddingTop: 0
  },
  appContainer: {
    //paddingTop: "60px"
  },
  loading: {},
  button: {
    marginTop: 15,
    width: "100%",
    maxWidth: 250,
    marginBottom: 10,
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    padding: "0px 16px",
    borderRadius: 2,
    boxShadow: "0px 1px 3px rgba(0, 0, 0, .3)",
    cursor: "pointer",
    outline: "none",
    border: "none",
    minHeight: 40
  },
  text: {
    color: "white",
    fontSize: 14,
    marginLeft: 10,
    fontWeight: "bold"
  },
  signOut: {
    backgroundColor: "black"
  },
  footer: {
    fontWeight: "600",
    padding: "0px 25px",
    textAlign: "right",
    height: "25px",
    color: "rgba(0, 0, 0, 0.6)"
  },
  anchor: {
    color: "rgb(255, 153, 0)",
    textDecoration: "none"
  },
  body: {
    position: "relative",
    margin: 0,
    //paddingBottom: "6rem",
    minHeight: "100%"
  }
};

export default withUserState(App);
