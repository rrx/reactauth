import React from "react";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// nodejs library that concatenates classes
import classNames from "classnames";
// material-ui core components
import { List, ListItem } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

import { primaryColor } from "assets/jss/material-kit-react.js";

import DebugPopup from "components/Debug/DebugPopup";

const styles = {
  a: {
    color: primaryColor,
    textDecoration: "none",
    backgroundColor: "transparent"
  },
  footerWhiteFont: {
    color: "white"
  },
  footer: {
    position: "absolute",
    right: "0",
    bottom: "0",
    left: "0",
    padding: "1rem",
    textAlign: "center"
  },
  left: {
    textAlign: "left"
    // float: "left"
  },
  right: {
    float: "right"
  },
  inlineBlock: {
    display: "inline-block"
  }
};

const useStyles = makeStyles(styles);

export default function Footer(props) {
  const classes = useStyles();
  const { whiteFont } = props;
  const footerClasses = classNames({
    [classes.footer]: true,
    [classes.footerWhiteFont]: whiteFont,
    [classes.a]: true
  });
  const aClasses = classNames({
    [classes.a]: true,
    [classes.footerWhiteFont]: whiteFont
  });
  return (
    <footer className={footerClasses}>
      <div className={classes.container}>
        <div className={classes.left}>
          <DebugPopup />
        </div>
        <div className={classes.right}>
          <List className={classes.list}>
            <ListItem className={classes.inlineBlock}>
              <a
                href="https://reacttest.aifuzz.com"
                className={aClasses}
                rel="noopener noreferrer"
                target="_blank"
              >
                AIFuzz
              </a>
            </ListItem>
          </List>
        </div>
        <div className={footerClasses}>
          &copy; {1900 + new Date().getYear()}{" "}
          <a
            href="https://reacttest.aifuzz.com"
            className={aClasses}
            rel="noopener noreferrer"
            target="_blank"
          >
            AIFuzz
          </a>
        </div>
      </div>
    </footer>
  );
}

Footer.propTypes = {
  whiteFont: PropTypes.bool
};
