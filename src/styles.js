import { makeStyles } from "@material-ui/core/styles";

const styles = makeStyles(theme => ({
  appBody: {
    paddingLeft: 25,
    paddingRight: 25
  },
  button: {
    margin: theme.spacing(1)
  },
  leftIcon: {
    marginRight: theme.spacing(1)
  },
  rightIcon: {
    marginLeft: theme.spacing(1)
  },
  iconSmall: {
    fontSize: 20
  }
}));

export default styles;
