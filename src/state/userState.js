import React from "react";
import PropTypes from "prop-types";

const initialUserState = { user: null, loading: true, authState: "start" };

function reducer(state, action) {
  switch (action.type) {
    case "logout":
      return { ...state, user: null };
    case "setUser":
      return { ...state, user: action.user, loading: false };
    case "loaded":
      return { ...state, loading: false };
    case "authState":
      return { ...state, authState: action.value };
    default:
      return state;
  }
}

export const Context = React.createContext(initialUserState);

export const StateProvider = ({ children }) => {
  const [userState, dispatch] = React.useReducer(reducer, initialUserState);
  const setAuthState = state => dispatch({ type: "authState", value: state });
  const setUser = user => dispatch({ type: "setUser", user: user });

  return (
    <Context.Provider value={[userState, dispatch, { setAuthState, setUser }]}>
      {children}
    </Context.Provider>
  );
};

StateProvider.propTypes = {
  children: PropTypes.object.isRequired
};

export const withUserState = Component => {
  return function StateProviderWrapper(props) {
    return (
      <StateProvider>
        <Component {...props} />
      </StateProvider>
    );
  };
};

export const useUserState = () => React.useContext(Context);
