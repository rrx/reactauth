import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import "typeface-roboto";
import { createBrowserHistory } from "history";

import Amplify from "aws-amplify";
import config from "./aws-exports-env";

Amplify.configure(config);
//Amplify.Logger.LOG_LEVEL = "DEBUG";

var hist = createBrowserHistory();

ReactDOM.render(<App history={hist} />, document.querySelector("#root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
