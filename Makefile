default:

build-production:
	yarn build

serve-production:
	npx serve -s build

push: build-production
	aws s3 cp --cache-control max-age=0,no-cache,no-store,must-revalidate --acl public-read --recursive build/ s3://rrx-reactauth-static/

lint:
	npm run lint:check

lint-fix:
	npm run lint:fix

test:
	JEST_JUNIT_OUTPUT_DIR=. CI=true yarn test --coverage --reporters="default" --reporters="jest-junit"
