import React from "react";
import PropTypes from "prop-types";
import { Formik } from "formik";

// Components
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import CardFooter from "components/Card/CardFooter.js";
import Button from "components/CustomButtons/Button.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import SimpleCard from "pages/SimpleCard/SimpleCard";

import { makeStyles } from "@material-ui/core/styles";
import { Auth } from "aws-amplify";

import styles from "assets/jss/material-kit-react/views/loginPage.js";

const useStyles = makeStyles(styles);

const validate = values => {
  let errors = {};
  if (!values.challenge) {
    errors.challenge = "Required";
  }
  return errors;
};

function MFA({ userState, onSuccess }) {
  const classes = useStyles();

  if (!userState.user) {
    return <div>Not logged in</div>;
  }

  return (
    <SimpleCard>
      <Formik
        initialValues={{ challenge: "" }}
        validate={validate}
        onSubmit={(values, { setSubmitting, setErrors, setStatus }) => {
          console.log("submit", values, setSubmitting);
          setSubmitting(false);
          setErrors({});
          setStatus(null);

          Auth.confirmSignIn(
            userState.user, // Return object from Auth.signIn()
            values.challenge, // Confirmation code
            userState.user.challengeName // MFA Type e.g. SMS_MFA, SOFTWARE_TOKEN_MFA
          )
            .then(loggedInUser => {
              console.log("loggedInUser", loggedInUser);
              setStatus("MFA Confirmation");
              onSuccess(loggedInUser);
            })
            .catch(e => {
              console.log("MFA Error", e);
              setErrors({ form: e.message });
            })
            .finally(() => setSubmitting(false));
        }}
      >
        {({
          values,
          errors,
          status,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting
          /* and other goodies */
        }) => (
          <form className={classes.form} onSubmit={handleSubmit}>
            <CardHeader color="primary" className={classes.cardHeader}>
              <h4>Multi Factor Authentication</h4>
            </CardHeader>
            <CardBody>
              <h5>Please enter the code 2FA device.</h5>
              {status && <div>{status}</div>}
              {errors.form && errors.form}

              <CustomInput
                labelText="Challenge..."
                id="challenge"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                  type: "text",
                  autoFocus: true,
                  onChange: handleChange,
                  onBlur: handleBlur,
                  value: values.challenge,
                  autoComplete: "off"
                }}
              />
              {errors.email && touched.email && errors.email}
            </CardBody>
            <CardFooter className={classes.cardFooter}>
              <Button
                type="submit"
                color="primary"
                size="lg"
                disabled={isSubmitting}
              >
                Submit
              </Button>
            </CardFooter>
          </form>
        )}
      </Formik>
    </SimpleCard>
  );
}

export default MFA;

MFA.propTypes = {
  userState: PropTypes.object,
  onSuccess: PropTypes.func
};
