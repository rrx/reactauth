exports.handler = (event, context, callback) => {
  // Define the URL that you want the user to be directed to after verification is complete
  if (event.triggerSource === "CustomMessage_SignUp") {
    const { codeParameter } = event.request;
    const { region, userName } = event;
    const { clientId } = event.callerContext;
    const redirectUrl = `${process.env.REDIRECTURL}auth/signupconfirm?username=${userName}&code=${codeParameter}`;
    const resourcePrefix = process.env.RESOURCENAME.split("CustomMessage")[0];

    const hyphenRegions = [
      "us-east-1",
      "us-west-1",
      "us-west-2",
      "ap-southeast-1",
      "ap-southeast-2",
      "ap-northeast-1",
      "eu-west-1",
      "sa-east-1"
    ];

    const seperator = hyphenRegions.includes(region) ? "-" : ".";

    const payload = Buffer.from(
      JSON.stringify({
        userName,
        redirectUrl,
        region,
        clientId
      })
    ).toString("base64");
    const bucketUrl = `http://${resourcePrefix}verificationbucket-${process.env.ENV}.s3-website${seperator}${region}.amazonaws.com`;
    const url = `${bucketUrl}/?data=${payload}&code=${codeParameter}`;
    const message = `${process.env.EMAILMESSAGE}. \n\n You will need this code to login: ${codeParameter}\n\n To confirm, click on the following link: \n\n${redirectUrl}`;
    event.response.smsMessage = message;
    event.response.emailSubject = process.env.EMAILSUBJECT;
    event.response.emailMessage = message;
    console.log("event.response", event.response);
    callback(null, event);
  } else {
    callback(null, event);
  }
};
