import React from "react";

import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import CardFooter from "components/Card/CardFooter.js";
import SimpleCard from "pages/SimpleCard/SimpleCard";

import Header from "components/Header/Header";
import pageStyles from "styles";

export default function About() {
  const classes = pageStyles();
  return (
    <div>
      <Header />
      <SimpleCard>
        <CardHeader color="primary" className={classes.cardHeader}>
          <h4>About</h4>
        </CardHeader>
        <CardBody>
          <p>This is a placeholder for the About Page</p>
        </CardBody>
        <CardFooter></CardFooter>
      </SimpleCard>
    </div>
  );
}
