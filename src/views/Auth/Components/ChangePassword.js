import React from "react";
import PropTypes from "prop-types";
import { Formik } from "formik";

// Components
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import CardFooter from "components/Card/CardFooter.js";
import Button from "components/CustomButtons/Button.js";
import InputAdornment from "@material-ui/core/InputAdornment";
import SimpleCard from "pages/SimpleCard/SimpleCard";
import AuthInput from "views/Auth/Components/AuthInput";

// @material-ui/icons
import Icon from "@material-ui/core/Icon";

import { makeStyles } from "@material-ui/core/styles";
import { Auth } from "aws-amplify";

import styles from "assets/jss/material-kit-react/views/loginPage.js";

const useStyles = makeStyles(styles);

const validate = values => {
  console.log("validate", values);
  let errors = {};
  if (!values.password) {
    errors.password = "Required";
  }

  if (!values.password_confirm) {
    errors.password_confirm = "Required";
  }

  if (
    values.password &&
    values.password_confirm &&
    values.password !== values.password_confirm
  ) {
    errors.form = "Passwords do not match";
  }
  return errors;
};

function ChangePassword(props) {
  const classes = useStyles();
  const { userState, onSuccess } = props;

  if (userState.loading || !userState.user) {
    return <div>Loading</div>;
  }

  if (!userState.loading && !userState.user) {
    return <div>Not logged in</div>;
  }

  return (
    <SimpleCard>
      <Formik
        initialValues={{ password_old: "", password: "", password_confirm: "" }}
        validate={validate}
        validateOnChange
        onSubmit={(values, { setSubmitting, setErrors, setStatus }) => {
          console.log("submit", userState.user, values);

          if (!userState.user) {
            return;
          }

          setErrors({});
          setStatus(null);
          setSubmitting(true);
          Auth.changePassword(
            userState.user, // the Cognito User Object
            values.password_old, // old password
            values.password // the new password
          )
            .then(user => {
              console.log(user);
              onSuccess(user);
            })
            .catch(e => {
              console.log(e);
              setErrors({ form: e.message });
            })
            .finally(() => setSubmitting(false));

          //onSuccess()
          // Auth.signIn(values.email, values.password)
          // .then( (user) => {
          //     console.log('user', user);
          //     setStatus('Logged In')
          //     onSuccess(user);
          // }).catch( (e) => {
          //     console.log('error', e);
          //     setErrors({form: e.message});
          // })
          // .finally( () => setSubmitting(false) )
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting
          /* and other goodies */
        }) => (
          <form className={classes.form} onSubmit={handleSubmit}>
            {console.log("form", values, errors)}
            <CardHeader color="primary" className={classes.cardHeader}>
              <h4>Change Password</h4>
            </CardHeader>
            <CardBody>
              {errors.form && errors.form}

              <AuthInput
                id="password_old"
                type="password"
                labelText="Old Password"
                autoComplete="off"
                autoFocus={false}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password_old}
                errors={errors}
                touched={touched}
                endAdornment={
                  <InputAdornment position="end">
                    <Icon className={classes.inputIconsColor}>
                      lock_outline
                    </Icon>
                  </InputAdornment>
                }
              />

              <AuthInput
                id="password"
                type="password"
                labelText="Password"
                autoComplete="off"
                autoFocus={false}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
                errors={errors}
                touched={touched}
                endAdornment={
                  <InputAdornment position="end">
                    <Icon className={classes.inputIconsColor}>
                      lock_outline
                    </Icon>
                  </InputAdornment>
                }
              />

              <AuthInput
                id="password_confirm"
                type="password"
                labelText="Confirm Password"
                autoComplete="off"
                autoFocus={false}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password_confirm}
                errors={errors}
                touched={touched}
                endAdornment={
                  <InputAdornment position="end">
                    <Icon className={classes.inputIconsColor}>
                      lock_outline
                    </Icon>
                  </InputAdornment>
                }
              />
            </CardBody>
            <CardFooter className={classes.cardFooter}>
              <Button
                type="submit"
                color="primary"
                size="lg"
                disabled={isSubmitting}
              >
                Submit
              </Button>
            </CardFooter>
          </form>
        )}
      </Formik>
    </SimpleCard>
  );
}

export default ChangePassword;

ChangePassword.propTypes = {
  userState: PropTypes.object,
  onSuccess: PropTypes.func
};
