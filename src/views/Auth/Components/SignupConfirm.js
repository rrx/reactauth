import React, { useState } from "react";
import PropTypes from "prop-types";
import { Formik } from "formik";
import { Redirect, withRouter } from "react-router-dom";
import queryString from "query-string";

// Components
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import CardFooter from "components/Card/CardFooter.js";
import Button from "components/CustomButtons/Button.js";
import SimpleCard from "pages/SimpleCard/SimpleCard";
import AuthInput from "views/Auth/Components/AuthInput";

import { makeStyles } from "@material-ui/core/styles";
import { Auth } from "aws-amplify";

import styles from "assets/jss/material-kit-react/views/loginPage.js";

const useStyles = makeStyles(styles);

function SignupConfirm(props) {
  const [confirmed, setConfirmed] = useState(false);
  const classes = useStyles();

  const { username, destination, location } = props;

  const params = queryString.parse(location.search);

  console.log("SignupConfirm", props);
  console.log("SignupConfirm params", params);

  if (confirmed) {
    return <Redirect to="/login" />;
  }

  function confirm(username, code) {
    Auth.confirmSignUp(username, code)
      .then(() => {
        console.log("signup confirm");
        setConfirmed(true);
      })
      .catch(e => {
        console.log("Error", e);
        switch (e.code) {
          case "NotAuthorizedException":
            setConfirmed(true);
            break;
          default:
            break;
        }
      });
  }

  if (params.code && params.username) {
    confirm(params.username, params.code);
  }

  return (
    <SimpleCard>
      <Formik
        initialValues={{
          username: username || params.username || "",
          code: params.username || ""
        }}
        validate={values => {
          console.log("validate", values);
          let errors = {};
          if (values.resend) {
            return errors;
          }

          if (!values.username) {
            errors.username = "Required";
          }
          if (!values.code) {
            errors.code = "Required";
          }

          return errors;
        }}
        onSubmit={(
          values,
          { setFieldValue, setSubmitting, setErrors, setStatus }
        ) => {
          console.log("submit", values);
          setSubmitting(false);
          setErrors({});
          setStatus(null);

          if (values.resend) {
            console.log("resend");
            Auth.resendSignUp(values.username)
              .then(data => {
                console.log("resend", data);
                setStatus("A code has been emailed");
              })
              .catch(e => {
                setErrors({ form: e.message });
                setStatus(e.message);
                console.log("resend error", e);
              })
              .finally(() => {
                setFieldValue("resend", false);
              });
          } else {
            // Collect confirmation code and new password, then
            Auth.confirmSignUp(values.username, values.code)
              .then(() => {
                console.log("signup confirm");
                setConfirmed(true);
              })
              .catch(e => {
                console.log("Error", e);
                setErrors({ form: e.message });
              })
              .finally(() => setSubmitting(false));
          }
        }}
      >
        {({
          values,
          errors,
          status,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          setFieldValue
          /* and other goodies */
        }) => (
          <form className={classes.form} onSubmit={handleSubmit}>
            {console.log("form", values)}
            <CardHeader color="primary" className={classes.cardHeader}>
              <h4>Signup Confirm</h4>
            </CardHeader>

            <CardBody>
              {destination && (
                <p>Something was sent to {destination}. Check your email.</p>
              )}

              {!destination && (
                <p>Something was sent to your email. Check your email.</p>
              )}

              {status && <div>{status}</div>}
              {errors.form && <p>{errors.form}</p>}

              <AuthInput
                id="username"
                labelText="Enter your email..."
                autoComplete="off"
                autoFocus={true}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.username}
                errors={errors}
                touched={touched}
              />

              <AuthInput
                id="code"
                labelText="Enter your confirmation code..."
                autoComplete="off"
                autoFocus={true}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.code}
                errors={errors}
                touched={touched}
              />
            </CardBody>
            <CardFooter className={classes.cardFooter}>
              <Button
                type="submit"
                color="primary"
                size="lg"
                disabled={isSubmitting}
              >
                Submit
              </Button>

              <Button
                color="primary"
                size="lg"
                disabled={isSubmitting}
                onClick={async e => {
                  e.persist();
                  await setFieldValue("resend", true);
                  handleSubmit(e);
                }}
              >
                Resend Code
              </Button>
            </CardFooter>
          </form>
        )}
      </Formik>
    </SimpleCard>
  );
}

export default withRouter(SignupConfirm);

SignupConfirm.propTypes = {
  username: PropTypes.string,
  destination: PropTypes.string,
  onSuccess: PropTypes.func,
  location: PropTypes.object.isRequired
};
