import React, { useReducer, useEffect, useRef } from "react";
import { Auth } from "aws-amplify";

import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
import SimpleCard, { SimpleCardHeader } from "pages/SimpleCard/SimpleCard";

import QRCode from "qrcode.react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";

const initialUserState = {
  user: null,
  loading: true,
  code: null,
  error: false,
  message: null,
  preferredMFA: null,
  mfa: null,
  mfaDisableError: false,
  loadFlags: new Set(),
  checkCodeValue: ""
};

function mapPreferredMFAToString(preferredMFA) {
  var out = null;
  switch (preferredMFA) {
    case "SOFTWARE_TOKEN_MFA":
    case "TOTP":
      out = "One Time Password";
      break;
    default:
      out = "MFA Not Setup";
      break;
  }
  return out;
}

function reducer(state, action) {
  // single command actions
  switch (action) {
    case "clearMessage":
      return { ...state, error: false, message: null };
    case "checkCodeClear":
      return { ...state, checkCodeValue: "" };
    case "loaded":
      return { ...state, loading: false };
    case "loading":
      return { ...state, loading: true };
    case "reset":
      return initialUserState;
    default:
      break;
  }

  switch (action.type) {
    case "setUser":
      console.log("setUser", action.user);
      if (action.user) {
        return {
          ...state,
          user: action.user,
          preferredMFA: mapPreferredMFAToString(action.user.preferredMFA)
        };
      } else {
        return { ...state, user: null };
      }
    case "setCode":
      return { ...state, code: action.code };
    case "setError":
      return { ...state, error: true, message: action.message };
    case "setMessage":
      return { ...state, error: false, message: action.message };
    case "updateMFA":
      state.user.preferredMFA = action.preferredMFA;
      return {
        ...state,
        code: null,
        preferredMFA: action.preferredMFA,
        mfa: mapPreferredMFAToString(action.preferredMFA)
      };
    case "mfaDisableError":
      return { ...state, mfaDisableError: action.value };
    case "loaderPush":
      state.loadFlags.add(action.key);
      return state;
    case "loaderPop":
      var flags = state.loadFlags;
      flags.delete(action.key);
      return { ...state, loadFlags: flags };
    default:
      return state;
  }
}

export default function MFASetup() {
  const [state, dispatch] = useReducer(reducer, initialUserState);
  let checkCodeValue = useRef("");

  const classes = useStyles();

  function removeMFA() {
    return Auth.setPreferredMFA(state.user, "NOMFA");
  }

  function addMFA() {
    return Auth.setPreferredMFA(state.user, "TOTP");
  }

  function setMFADisableError(value) {
    dispatch({ type: "mfaDisableError", value: value });
  }

  function setMessage(message) {
    dispatch({ type: "setMessage", message: message });
  }

  function setError(message) {
    dispatch({ type: "setError", message: message });
  }

  function clearMessage() {
    dispatch("clearMessage");
  }

  function loaderPush(key) {
    dispatch({ type: "loaderPush", key: key });
  }

  function loaderPop(key) {
    dispatch({ type: "loaderPop", key: key });
  }

  // function isLoadingKey(key) {
  //   return state.loadFlags.has(key);
  // }

  useEffect(() => {
    if (state.user) return;
    if (state.code) return;
    Auth.currentAuthenticatedUser().then(user => {
      dispatch({ type: "setUser", user: user });
    });

    //loading()
  });

  function verifyMFA(challengeAnswer) {
    console.log("verify", state.user, challengeAnswer);
    loaderPush("checkCode");
    Auth.verifyTotpToken(state.user, challengeAnswer)
      .then(() => {
        addMFA().then(() => {
          dispatch({ type: "updateMFA", preferredMFA: "SOFTWARE_TOKEN_MFA" });
        });
        console.log("verified");
      })
      .catch(e => {
        dispatch({ type: "setError", message: e.message });
        console.log("error", e);
      })
      .finally(() => loaderPop("checkCode"));
  }

  function actionSetupMFA() {
    if (!state.user) return;
    Auth.setupTOTP(state.user).then(code => {
      const username = state.user.username;
      const issuer = "aifuzz.com";
      dispatch({
        type: "setCode",
        code:
          "otpauth://totp/AWSCognito:" +
          username +
          "?secret=" +
          code +
          "&issuer=" +
          issuer
      });
    });
  }

  function actionRemoveMFA() {
    console.log("remove", state.user);
    if (!state.user) return;
    removeMFA()
      .then(() => {
        dispatch({ type: "updateMFA", preferredMFA: null });
      })
      .catch(e => {
        console.log("error", e);
        dispatch({ type: "setCode", code: null });
        setMFADisableError(true);
      });
  }

  function actionCancelCode() {
    dispatch({ type: "setCode", code: null });
  }

  const isDisabled = state.loadFlags.has("checkCode");

  return (
    <SimpleCard>
      <SimpleCardHeader>
        <h4>MFA Setup</h4>
      </SimpleCardHeader>
      <CardBody>
        {console.log("state", state, state.loadFlags, isDisabled)}
        {state.user && <h5>User: {state.user.attributes.email}</h5>}

        {!state.user && (
          <div>
            <h2>MFA Setup</h2>
            <p>Loading User</p>
          </div>
        )}

        {state.mfaDisableError && (
          <div>
            There was an error attempting to disable MFA. To disable MFA, you
            will need to successfully re-add MFA, before you can disable it.
          </div>
        )}

        {state.user && (
          <div>
            {state.code && (
              <div>
                <div>
                  <QRCode value={state.code} />
                </div>
                <div>{state.code}</div>
                <div>
                  <TextField
                    required
                    id="outlined-required"
                    label="Enter Code"
                    error={state.error}
                    helperText={state.message}
                    defaultValue=""
                    className="awesome"
                    margin="normal"
                    variant="outlined"
                    onKeyPress={e => {
                      if (e.key === "Enter") verifyMFA(e.target.value);
                    }}
                  />
                </div>
                <div>
                  <Button
                    variant="contained"
                    color="secondary"
                    onClick={actionCancelCode}
                    className={classes.button}
                  >
                    Cancel
                  </Button>
                </div>
              </div>
            )}

            {!state.code && state.user.preferredMFA === "SOFTWARE_TOKEN_MFA" && (
              <div>
                <p>MFA is setup for this account</p>
              </div>
            )}

            {!state.code && state.user.preferredMFA === "NOMFA" && (
              <div>
                <p>MFA is not setup for this account</p>
              </div>
            )}

            {!state.mfaDisableError &&
              !state.code &&
              state.user.preferredMFA === "SOFTWARE_TOKEN_MFA" && (
                <div>
                  <p>
                    To disable, click
                    <Button
                      variant="contained"
                      color="secondary"
                      onClick={actionRemoveMFA}
                      className={classes.button}
                    >
                      Remove MFA
                    </Button>
                  </p>
                </div>
              )}

            {!state.code && (
              <p>
                To update or add MFA to your account:
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={actionSetupMFA}
                  className={classes.button}
                >
                  Add MFA
                </Button>
              </p>
            )}
          </div>
        )}

        {!state.code && (
          <div>
            <TextField
              required
              disabled={isDisabled}
              id="outlined-required"
              label="Check Code"
              inputRef={checkCodeValue}
              error={state.error}
              helperText={state.message}
              className="awesome"
              margin="normal"
              variant="outlined"
              onKeyPress={e => {
                const answer = e.target.value;
                if (e.key === "Enter") {
                  clearMessage();
                  loaderPush("checkCode");
                  Auth.verifyTotpToken(state.user, answer)
                    .then(() => {
                      setMessage("Success");
                      checkCodeValue.current.value = "";
                    })
                    .catch(e => {
                      setError(e.message);
                      console.log("error", e);
                    })
                    .finally(() => loaderPop("checkCode"));
                }
              }}
            />
          </div>
        )}
      </CardBody>
      <CardFooter></CardFooter>
    </SimpleCard>
  );
}

const useStyles = makeStyles(theme => ({
  button: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1)
  }
}));
