import React from "react";

import { makeStyles } from "@material-ui/core/styles";

import { withHeaderState } from "state/headerState";

// icons and elements
import LeftNav from "./LeftNav";
import TopBar from "./TopBar";

const Header = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <TopBar />
      <LeftNav />
    </div>
  );
};

const useStyles = makeStyles({
  root: {
    //display: 'flex',
  }
});

export default withHeaderState(Header);
