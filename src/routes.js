import React from "react";
import PropTypes from "prop-types";

import InboxIcon from "@material-ui/icons/MoveToInbox";
import { Route, Switch } from "react-router-dom";

// Load Components
import Home from "./Home";
import Upload from "./Upload";
import Header from "components/Header/Header";
import ProfileUploader from "./ProfileUploader";
import Auth from "./views/Auth/Auth";
import About from "views/About/About";
import RouterDemo from "views/RouterDemo/RouterDemo";
import NotFound from "views/NotFound/NotFound";

// pages for this product
import Components from "views/Components/Components";
import LandingPage from "views/LandingPage/LandingPage.js";
import ProfilePage from "views/ProfilePage/ProfilePage.js";
import SimpleLandingPage from "pages/SimpleLandingPage/SimpleLandingPage";

// state
import { useUserState } from "state/userState";

const routeList = [
  {
    key: "/",
    name: "Home",
    component: HomeWrapper,
    icon: <InboxIcon />,
    auth: false
  },
  {
    key: "/totpsetup",
    name: "MFA Setup",
    component: MFASetup,
    icon: <InboxIcon />,
    auth: true
  },
  {
    key: "/password",
    name: "Change Password",
    component: ChangePassword,
    icon: <InboxIcon />,
    auth: true
  },
  {
    key: "/about",
    name: "About",
    component: About,
    icon: <InboxIcon />,
    auth: false
  },
  {
    key: "/upload1",
    name: "Upload1",
    component: Upload,
    icon: <InboxIcon />,
    auth: true
  },
  {
    key: "/upload2",
    name: "Upload2",
    component: ProfileUploader,
    icon: <InboxIcon />,
    auth: true
  },
  {
    key: "/routerdemo",
    name: "RouterDemo",
    component: RouterDemo,
    icon: <InboxIcon />,
    auth: false
  },
  {
    key: "/routerdemo/:id",
    name: "RouterDemo",
    component: RouterDemo,
    icon: null,
    auth: false
  },
  {
    key: "/landing-page",
    name: "Landing Page",
    component: LandingPage,
    icon: null,
    auth: false
  },
  {
    key: "/profile-page",
    name: "Profile Page",
    component: ProfilePage,
    icon: null,
    auth: false
  },
  {
    key: "/components",
    name: "Components Page",
    component: Components,
    icon: <InboxIcon />,
    auth: false
  }
];

// const routeList2 = [
//   ["/auth", "Login", Auth, null, false],
//   ["/login", "Login", Auth, null, false],
//   ["/signup", "SignUp", Signup, null, false],
//   ["/forgot", "Forgot", Forgot, null, false],
//   ["/password", "Change Password", ChangePassword, null, false],
// ];

function HomeWrapper(props) {
  return (
    <div>
      <Header />
      <SimpleLandingPage title="It's demo time">
        <Home {...props} />
      </SimpleLandingPage>
    </div>
  );
}

HomeWrapper.propTypes = {
  match: PropTypes.object.isRequired
};

export function routes(isLoggedIn) {
  return routeList
    .filter(values => !values.auth || (isLoggedIn && values.auth))
    .map(values => {
      const { key, name, component, icon } = values;
      return { key, name, component, icon };
    });
}

function ChangePassword() {
  return <Auth state="changepassword" />;
}

function MFASetup() {
  return <Auth state="mfasetup" />;
}

function AuthRoute({ path, state }) {
  return (
    <Route
      exact
      path={path}
      render={() => {
        return <Auth state={state} />;
      }}
    />
  );
}
AuthRoute.propTypes = {
  path: PropTypes.string.isRequired,
  state: PropTypes.string.isRequired
};

function AuthWithState({ match }) {
  return <Auth state={match.params.id} />;
}
AuthWithState.propTypes = {
  match: PropTypes.object.isRequired
};

export function Routes() {
  const [userState] = useUserState();

  const isLoggedIn = userState.user != null;

  return (
    <Switch>
      {routes(isLoggedIn).map(({ key, component }, index) => {
        return <Route exact key={index} path={key} component={component} />;
      })}

      <Route exact path="/auth/:id" component={AuthWithState} />
      <AuthRoute path="/auth" state="start" />

      <AuthRoute path="/login" state="login" />
      <AuthRoute path="/signup" state="signup" />
      <AuthRoute path="/forgot" state="forgot" />
      <AuthRoute path="/forgot/confirm" state="forgotconfirm" />
      <AuthRoute path="/password" state="changepassword" />

      <Route exact component={NotFound} />
    </Switch>
  );
}

export default routes;
