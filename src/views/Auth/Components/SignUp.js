import React from "react";
import PropTypes from "prop-types";
import { Formik } from "formik";
import { Link } from "react-router-dom";

// Components
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import CardFooter from "components/Card/CardFooter.js";
import Button from "components/CustomButtons/Button.js";
import InputAdornment from "@material-ui/core/InputAdornment";
import CustomInput from "components/CustomInput/CustomInput.js";
import SimpleCard from "pages/SimpleCard/SimpleCard";

// @material-ui/icons
import Icon from "@material-ui/core/Icon";
import Email from "@material-ui/icons/Email";

import { makeStyles } from "@material-ui/core/styles";
import { Auth } from "aws-amplify";

import styles from "assets/jss/material-kit-react/views/loginPage.js";
import { primaryColor } from "assets/jss/material-kit-react";

const useStyles = makeStyles(styles);

function SignUp({ onSuccess, validate }) {
  const classes = useStyles();

  return (
    <SimpleCard>
      <Formik
        initialValues={{ email: "", password: "" }}
        validate={validate}
        onSubmit={(values, { setSubmitting, setErrors, setStatus }) => {
          console.log("submit", values, setSubmitting);
          setSubmitting(false);
          setErrors({});
          setStatus(null);

          Auth.signUp({
            username: values.email,
            password: values.password
          })
            .then(user => {
              console.log("user", user);
              setStatus("Signed Up");
              onSuccess(user);
            })
            .catch(e => {
              console.log("error", e);
              setErrors({ form: e.message });
            })
            .finally(() => setSubmitting(false));
        }}
      >
        {({
          values,
          errors,
          status,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting
          /* and other goodies */
        }) => (
          <form className={classes.form} onSubmit={handleSubmit}>
            <CardHeader color="primary" className={classes.cardHeader}>
              <h4>Sign Up</h4>
            </CardHeader>
            <CardBody>
              {status && <div>{status}</div>}
              {errors.form && errors.form}

              <CustomInput
                labelText="Email..."
                id="email"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                  type: "email",
                  autoFocus: true,
                  onChange: handleChange,
                  onBlur: handleBlur,
                  value: values.email,
                  endAdornment: (
                    <InputAdornment position="end">
                      <Email className={classes.inputIconsColor} />
                    </InputAdornment>
                  )
                }}
              />
              {errors.email && touched.email && errors.email}
              <CustomInput
                labelText="Password"
                id="password"
                formControlProps={{
                  fullWidth: true
                }}
                inputProps={{
                  type: "password",
                  onChange: handleChange,
                  onBlur: handleBlur,
                  value: values.password,
                  endAdornment: (
                    <InputAdornment position="end">
                      <Icon className={classes.inputIconsColor}>
                        lock_outline
                      </Icon>
                    </InputAdornment>
                  ),
                  autoComplete: "off"
                }}
              />
              {errors.password && touched.password && errors.password}
            </CardBody>
            <CardFooter className={classes.cardFooter}>
              <Button
                type="submit"
                color="primary"
                size="lg"
                disabled={isSubmitting}
              >
                Sign Up
              </Button>
            </CardFooter>

            <div className="links" style={localStyle.links}>
              <Link style={localStyle.links} to="/auth">
                Already have an account? Login
              </Link>
            </div>
          </form>
        )}
      </Formik>
    </SimpleCard>
  );
}

const localStyle = {
  links: {
    paddingLeft: "25px",
    paddingRight: "25px",
    paddingBottom: "25px",
    color: primaryColor
  }
};

export default SignUp;

SignUp.propTypes = {
  onSuccess: PropTypes.func,
  validate: PropTypes.func
};
